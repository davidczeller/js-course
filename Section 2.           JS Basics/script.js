/*
const Mark = {
  mass: 78,
  height: 1.69,
}

const John = {
  mass: 92,
  height: 1.95,
}

const MarksBMI = Mark.mass / (Mark.height * Mark.height);
const JohnsBMI = John.mass / (John.height * John.height);

const higherBMI = MarksBMI > JohnsBMI 
? 'Marks BMI is higher' 
: 'Johns BMI is higher';

console.log('Marks BMI is: ' + MarksBMI + ' and ' + 'Johns BMI is: ' + JohnsBMI + ". So " + higherBMI);
*/

/*
const mike = {
  first: 89,
  second: 120,
  third: 103,
}

const john = {
  first: 116,
  second: 94,
  third: 123,
}

const mary = {
  first: 97,
  second: 134,
  third: 105,
}

const mikesTeamAvg = (mike.first + mike.second + mike.third) / 3;
const johnsTeamAvg = (john.first + john.second + john.third) / 3;
const marysTeamAvg = (mary.first + mary.second + mary.third) / 3;

//draw on the first place is missing...


if (mikesTeamAvg > johnsTeamAvg && mikesTeamAvg > marysTeamAvg) {
  console.log(`Mike\'s team wins with: ${mikesTeamAvg} pts!`);

  if (johnsTeamAvg > marysTeamAvg) console.log(`John\'s team is the second with: ${johnsTeamAvg} pts! Mary\s team finished at the third place with: ${marysTeamAvg} pts!`);
  else if (johnsTeamAvg < marysTeamAvg) console.log(`Mary\'s team is the second with: ${marysTeamAvg} pts! John\'s team finished at the third place with: ${johnsTeamAvg} pts!`);
  else console.log(`It\s a draw at the second place, because the scores are ${johnsTeamAvg} - ${marysTeamAvg}`);

} else if (mikesTeamAvg < johnsTeamAvg && marysTeamAvg < johnsTeamAvg) {
  console.log(`John\'s team wins with: ${johnsTeamAvg} pts!`);

  if (mikesTeamAvg > marysTeamAvg) console.log(`Mike\'s team is the second with: ${mikesTeamAvg} pts! Mary\s team finished at the third place with: ${marysTeamAvg} pts!`);
  else if (mikesTeamAvg > marysTeamAvg) console.log(`Mary\'s team is the second with: ${marysTeamAvg} pts! Mike\'s team finished at the third place with: ${mikesTeamAvg} pts!`);
  else console.log(`It\s a draw at the second place, because the scores are ${mikesTeamAvg} - ${marysTeamAvg}`);

} else if ((marysTeamAvg > johnsTeamAvg && marysTeamAvg > mikesTeamAvg)) {
  console.log(`Mary\'s team wins with: ${marysTeamAvg} pts!`);

  if (johnsTeamAvg > mikesTeamAvg) console.log(`John\'s team is the second with: ${johnsTeamAvg} pts! Mike\'s team finished at the third place with: ${mikesTeamAvg} pts!`);
  else if (johnsTeamAvg < mikesTeamAvg) console.log(`Mike\'s team is the second with: ${mikesTeamAvg} pts! John\'s team finished at the third place with: ${johnsTeamAvg} pts!`);
  else console.log(`It\s a draw at the second place, because the scores are ${johnsTeamAvg} - ${mikesTeamAvg}`);

} else {
  console.log(`It\s a draw, because the scores are ${mikesTeamAvg} - ${johnsTeamAvg} - ${marysTeamAvg}`)
}
*/

/*
const tips = [];
const totalAmount = [];
const firstBill = 124;
const secondBill = 48;
const thirdBill = 268;

const tipCalc = (amount) => {
  if (amount < 50) return amount * 0.2;
  else if (amount >= 50 && amount <= 200) return amount * 0.15;
  else return amount * 0.1;
}

const first = tipCalc(firstBill);
const second = tipCalc(secondBill);
const third = tipCalc(thirdBill);

const totalFirst = firstBill + first;
const totalSecond = secondBill + second;
const totalThird = thirdBill + third;

tips.push(first, second, third);
totalAmount.push(totalFirst, totalSecond, totalThird);

console.log({
  first,
  second,
  third,
  totalFirst,
  totalSecond,
  totalThird,
  tips,
  totalAmount
});
*/

/*
const mark = {
  fullName: 'Mark Booger',
  mass: 78,
  height: 1.69,
  marksBMI: function () {
    this.bmi = this.mass / (this.height * this.height);
    return this.bmi;    
  }
}

const john = {
  fullName: 'John Smith',
  mass: 92,
  height: 1.95,
  johnsBMI: function () {
    this.bmi = this.mass / (this.height * this.height);
    return this.bmi;
  }
}

// mark.marksBMI();
// console.log(`Mark\'s BMI is: ${mark.bmi}`);

// john.johnsBMI();
// console.log(`John\'s BMI is: ${john.bmi}`);

if(john.johnsBMI() > mark.marksBMI()) {
  console.log(`${john.fullName} has a higher BMI of ${john.bmi}`);
} else if (john.bmi < mark.bmi) {
  console.log(`${mark.fullName} has a higher BMI of ${mark.bmi}`);
} else {
  console.log('They have the same BMI!')
}
*/

/*
const john = {
  bills: [124, 48, 268, 180, 42],
  tipCalc: function () {
    this.tips = [];
    this.totalAmount = [];

    for (let i = 0; i < this.bills.length; i++) {
      let percentage;
      let bill = this.bills[i];

      if (bill < 50) {
        percentage = 0.2;
      } else if (bill >= 50 && bill <= 200) {
        percentage = 0.15;
      } else {
        percentage = .1;
      }
      this.tips[i] = bill * percentage;
      this.totalAmount[i] = bill + bill * percentage;
    }
  }
}

const mark = {
  bills: [77, 375, 110, 45],
  tipCalc: function () {
    this.tips = [];
    this.totalAmount = [];

    for (let i = 0; i < this.bills.length; i++) {
      let percentage;
      let bill = this.bills[i];

      if (bill < 100) percentage = .20;
      else if (bill >= 100 && bill <= 300) percentage = .10;
      else percentage = .25;
      this.tips[i] = bill * percentage;
      this.totalAmount[i] = bill + bill * percentage;
    }
  }
}

const calcAvg = tips => {
  let sum = 0;
  for (let i = 0; i < tips.length; i++) {
    sum = sum + tips[i];
  }
  return sum / tips.length;
}

john.tipCalc();
mark.tipCalc();

john.avg = calcAvg(john.tips);
mark.avg = calcAvg(mark.tips);

if (john.avg > mark.avg) {
  console.log(`John'\s family pays more tips with avarage of: ${john.avg}`)
} else if (john.avg < mark.avg) {
  console.log(`Mark'\s family pays more tips with avarage of: ${mark.avg}`)
} else {
  console.log('They pay the same amount of tips.')
}
*/
