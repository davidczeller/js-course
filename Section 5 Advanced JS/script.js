/* 
!!!! THIS keyword binding, call, apply, new !!!!

https://tylermcginnis.com/this-keyword-call-apply-bind-javascript/

*/

/////////// ///////// ///////// ///////// ///////// ///////// /////////        INHERITANCE       ///////// ///////// ///////// ///////// ///////// ///////// ///////// ///////// ///////// ///////// ///////// 



// ///////// ///////// ///////// ///////// ///////// Function constractor PREFERED against Object.create ///////// ///////// ///////// ///////// ///////// 
/*

const Person = function(name, yearOfBirth, job) {
  this.name = name;
  this.yearOfBirth = yearOfBirth;
  this.job = job;
  // this.calcAge = function() {
    //   console.log(2020 - this.yearOfBirth);
    // }
  }
  
  console.log(Person); //logs the person func 
  
  Person.prototype.calcAge = function() { // NOT in the obect anymore but we have access to it because it is in the prototype property of the function constructor.
    console.log(2020 - this.yearOfBirth);
  }

  Person.prototype.company = 'Dave Co'; 


  //Prototype chain: 
      //function constructor => object function connstructor
      //because the function constructor is an instance of the object function constructor.


// instantiation
const john = new Person('John', 1992, 'developer'); // new obect it's been created and then the func is called (new execution context created). 
const mary = new Person('Mary', 1991, 'designer'); 
const mark = new Person('Mark', 1987, 'lead dev'); 

john.calcAge();
mark.calcAge();
mary.calcAge();

console.log(mary.hasOwnProperty('company')); //it's gonna be false, because this is the property of the object not the function
console.log(mary.hasOwnProperty('job'));     //it's gonna be true, because this is the property of the function not the object

console.log(Person);   //logs the person func 
console.log(john);     // logs the Person object

*/




/////////// ///////// /////////  Object.create ///////// ///////// ///////// ///////// ///////// ///////// 

/*

const personProto = {
  calcAge: function () {
    console.log(2020 - this.yearOfBirth);
  }
}

// const john = Object.create(personProto);
// john.name = 'John';
// john.yearOfBirth = 1992;

// OR

const john = Object.create(personProto, {
  name: {value: 'John'},
  yearOfBirth: {value: 1991},
})

*/




/////////// ///////// ///////// Primitives vs Objects ///////// ///////// ///////// ///////// ///////// 

/*

//    primitive variable: hold the data inside of the variable
//    object variable: contain a reference which is points to the place in memory


//Primitives
let a = 2;
const b = a;
a = 46;

console.log({a,b}) // a = 46, b = 2

//Objects
let obj1 = {
  name: 'John',
}

const obj2 = obj1; //here is only a reference been created, not a new object!!

obj1.name = 'Johnny';
 
console.log(obj1, obj2); // obj1 = Johnny, obj2 = Johnny

//Functions

const age = 28;
const obj = {
  name: 'Dave',
  city: 'Vienna' 
}

function change(a, b) {
  a = 30;               // doesn't change it
  b.city = 'New York'   //changes the reference
}

change(age, obj)

console.log(age, obj.city); // age unchanged, city is changed


*/





///////// ///////// ///////// /////// Passing functions as args ///////// ///////// ///////// ///////// ///////// 

/*


const years = [1990, 1992, 1993, 1996, 2010];

function arrayCalc(arr, fn) {
  const arrResult = [];
  for (let i = 0; i < arr.length; i++) {
    arrResult.push(fn(arr[i]))
  }
  return arrResult;
}

function calcAge(element) {
  return 2020 - element;
}

function isOver18(element) {
  return element >= 18;
}

function maxHeartRate(el) {
  if (el >= 18 && el <= 81) {
    return Math.round(206.9 - (0.67 * el));
  } else return -1;
}

const ages = arrayCalc(years, calcAge);
const fullAges = arrayCalc(ages, isOver18);
const rates = arrayCalc(ages, maxHeartRate);

console.log(ages, fullAges, rates);

*/


///// ///////// ///////// ///////// ///////// ///////// Function returning functions ///////// ///////// ///////// ///////// ///////// 

/*

function interviewQuest(job) {
  if (job === 'designer') {
    return function (name) {
      console.log(name + ', can you pls explaine what UX is?')
    }
  } else if (job === 'teacher') {
    return function (name) {
      console.log(name + ' what subject do you teach?')
    }
  } else return function (name) {
    console.log('Hello ' + name + ' what do you do?')
  }
}

const teacherQuest = interviewQuest('teacher');
const designerQuest = interviewQuest('designer');

teacherQuest('Béla');
designerQuest('Mike');
interviewQuest('player')('Sarah');

*/

////// ///////// ///////// ///////// ///////// IIFE  Immediately Invoked Function Expression ///////// ///////// ///////// ///////// ///////// 

/*

function game() {
  const score = (Math.random() * 10);
  console.log(score >= 5)
}

game();

(function () {
  const score = (Math.random() * 10);
  console.log(score <= 5)
})();

(function (luck) {
  console.log(score >= 5 - luck)
})(5);

console.log(score);

*/


////// ///////// ///////// ///////// ///////// ///////// Closures ///////// ///////// ///////// ///////// ///////// 
/* 

!!!!!! An inner function always has access to the variables and parameters of its outer function, even after the outer function has returned. !!!!! 


!!!!!! Scope chain always stays intact !!!!!

*/

/*

function retirement(retirementAge) {
  const a = ' years left until retirement!'
  return function (yearOfBirth) {
    const age = 2020 - yearOfBirth;
    console.log((retirementAge - age) + a)
  }
}

// retirement(66)(1992)
const US = retirement(66);
const Germany = retirement(65);
const Hungary = retirement(70);
const Iceland = retirement(67);

US(1992)
Germany(1992)
Hungary(1992)
Iceland(1992)


function interviewQuest(job) {
  let question;
  return function (name) {
    if (job === 'designer') question = ', can you pls explaine what UX is?'
    else if (job === 'teacher') question = ', what subject do you teach?'
    else question = '! What are you doing?'
    console.log(name + question)
  }
}

interviewQuest('teacher')('Kate')
interviewQuest('mechanic')('Johnny')
interviewQuest('designer')('Dave')


const teacher = interviewQuest('teacher');
const mechanic = interviewQuest('mechanic');
const designer = interviewQuest('designer');

teacher('Kate');
mechanic('Johnny');
designer('Dave');

*/





/////// ///////// ///////// ///////// ///////// Bind, call and apply ///////// ///////// ///////// ///////// 

/*





const mike = {
  name: 'Mike',
  age: 28,
  job: 'developer',
  presentation: function (style, timeOfDay) {
    if (style === 'formal') console.log(
      `Good ${timeOfDay} Ladies and Gentleman! I'm ${this.name}, I'm a ${this.job} and I'm ${this.age} years old.`
    )
    else if (style === 'friendly') console.log(
      `Hey, what's up? I'm ${this.name}, I'm a ${this.job} and I'm ${this.age} years old. Have a nice ${timeOfDay}!`
    )
  }
}

const emily = {
  name: 'Emily',
  age: 24,
  job: 'designer',
}

mike.presentation('friendly', 'day')
mike.presentation.call(emily, 'formal', 'day') // 'call' allowes us to set the this variable 
mike.presentation.apply(emily, ['formal', 'day']) // 'apply' same as 'call', but with array as a second argument

const mikeFriendly = mike.presentation.bind(mike, 'friendly') //  Carrying  // 'bind' is allowes us to preset args
const emilyFormal = mike.presentation.bind(emily, 'formal')

mikeFriendly('morning');
mikeFriendly('evening');

emilyFormal('afternoon');
emilyFormal('night');



const years = [1990, 1992, 1993, 1996, 2010];

function arrayCalc(arr, fn) {
  const arrResult = [];
  for (let i = 0; i < arr.length; i++) {
    arrResult.push(fn(arr[i]))
  }
  return arrResult;
}

function calcAge(element) {
  return 2020 - element;
}

function isOver18(limit, element) {
  return element >= limit;
}

const ages = arrayCalc(years, calcAge)
const fullJapan = arrayCalc(ages, isOver18.bind(this, 20))

console.log(fullJapan, ages)


*/



///////// ///////// ///////// /////////  LEXICAL Binding ///////// ///////// ///////// ///////// 


/*




const user = {
  name: 'Tyler',
  age: 27,
  languages: ['JavaScript', 'Ruby', 'Python'],
  greet() {
    const hello = `Hello, my name is ${this.name} and I know`

    const langs = this.languages.reduce(function (str, lang, i) {
      if (i === this.languages.length - 1) {
        return `${str} and ${lang}.`
      }
      return `${str} ${lang},`
      // }, "") // throws an error : Uncaught TypeError: Cannot read property 'length' of undefined      
      // We never actually see the invocation of our anonymous function since JavaScript does that itself in the implementation of .reduce.
      // That’s the problem. We need to specify that we want the anonymous function we pass to .reduce to be invoked in the context of user.
      // That way this.languages will reference user.languages. As we learned above, we can use .bind.


    }.bind(this), "") // works just fine 

    alert(hello + langs)
  }
}


// There is a better way to doing it. 

// Again the reason for this because with arrow functions, this is determined “lexically”. 
// Arrow functions don’t have their own this. 
// Instead, just like with variable lookups, the JavaScript interpretor will look to the enclosing (parent) scope to determine what this is referencing.

const user2 = {
  name: 'Tyler',
  age: 27,
  languages: ['JavaScript', 'Ruby', 'Python'],
  greet() {
    const hello = `Hello, my name is ${this.name} and I know`

    const langs = this.languages.reduce((str, lang, i) => { // use arrow function 
      // Arrow functions ”this is determined lexically. That’s a fancy way of saying this is determined how you’d expect, following the normal variable lookup rules.”

      if (i === this.languages.length - 1) {
        return `${str} and ${lang}.`
      }
      return `${str} ${lang},`
    }, "")

    alert(hello + langs)
  }
}

user.greet()
user2.greet()

*/



///// ///////// ///////// ///////// ///////// /////////  Window binding ///////// ///////// ///////// ///////// ///////// ///////// 

/*

function sayAge () {
  console.log(`My age is ${this.age}`)
}

const user3 = {
  name: 'Tyler',
  age: 27
}

sayAge(); // My age is undefined !!!

// What you’d get is, unsurprisingly, My age is undefined because this.age would be undefined. 
// Here’s where things get a little weird. What’s really happening here is because there’s nothing to the left of the dot,
// we’re not using .call, .apply, .bind, or the new keyword, JavaScript is defaulting this to reference the window object. 
// What that means is if we add an age property to the window object, then when we invoke our sayAge function again, this.age will no longer be undefined but instead, 
// it’ll be whatever the age property is on the window object. Don’t believe me? Run this code,


window.age = 27

function sayAge2 () {
  console.log(`My age is ${this.age}`)
}

sayAge2()

*/