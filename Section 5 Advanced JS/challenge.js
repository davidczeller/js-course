(function () {

  let finishScore;

  function Question(question, answer, correctAnswer) {
    this.question = question;
    this.answer = answer;
    this.correctAnswer = correctAnswer;
  }

  Question.prototype.displayQuestion = function () {
    console.log(this.question)

    for (let i = 0; i < this.answer.length; i++) {
      console.log(i + ': ' + this.answer[i])
    }
  }

  Question.prototype.checkAnswer = function (ans, callback) {
    let sc;
    if (ans === this.correctAnswer) {
      console.log('Correct!')
      sc = callback(true);
    } else {
      console.log('Wrong!')
      sc = callback(false);
    }
    this.displayScore(sc);
  }


  Question.prototype.displayScore = function (score) {
    console.log(`--------------------`)
    console.log(`  Your score is: ${score}`)
    console.log(`--------------------`)
  }


  let sc = 0;

  function score() {
    return function (correct) {
      if (correct) {
        sc++
      }
      return sc;
    }
  }

  const actualScore = score();

  function nextQuestion() {
    const questions = [q1, q2, q3, q4];

    const randomNumber = Math.floor(Math.random() * questions.length);

    questions[randomNumber].displayQuestion();

    const answer = prompt('Enter the correct answer!');

    if (answer !== 'exit') {
      questions[randomNumber].checkAnswer(parseInt(answer), actualScore);
      if (sc < finishScore) {
        nextQuestion()
      } else {
        console.log(`********************`)

        console.log('*     YOU WON!     *')

        console.log(`********************`)
      }
    }
  }

  const q1 = new Question('Are you learning?', ['Yes', 'No'], 0);
  const q2 = new Question('Are you happy?', ['No', 'Yes'], 1);
  const q3 = new Question('Are you hungry?', ['Yes', 'No'], 1);
  const q4 = new Question('Are you working?', ['Yes', 'No'], 1);

  const goal = prompt('What should be the winning number?')
  if (goal) {
    finishScore = goal;
  }

  nextQuestion();
})();