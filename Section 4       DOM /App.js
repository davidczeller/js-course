/*
GAME RULES:
- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game
*/

var scores, roundScore, activePlayer, gamePlaying;
let finalScore = 100;

init();


document.querySelector('.btn-roll').addEventListener('click', function () {
  if (gamePlaying) {
    // 1. Random number
    var dice1 = Math.floor(Math.random() * 6) + 1;
    var dice2 = Math.floor(Math.random() * 6) + 1;

    //2. Display the result
    var dice1DOM = document.getElementById('dice-1');
    var dice2DOM = document.getElementById('dice-2');
    dice1DOM.style.display = dice2DOM.style.display = 'block';
    dice1DOM.src = 'dice-' + dice1 + '.png';
    dice2DOM.src = 'dice-' + dice2 + '.png';

    //3. Update the round score IF the rolled number was NOT a 1
    // if (dice1 !== 1 && dice2 !== 1) {
    //   //Add score
    //   roundScore += (dice1 + dice2);
    //   document.querySelector('#current-' + activePlayer).textContent = roundScore;
    // } else {
    //   //Next player
    //   nextPlayer();
    // }

    //4. Update the round score IF the rolled number was NOT a 1 or there were no 2 sixes.
    if (dice1 === 1 || dice2 === 1) {
      document.getElementById('error-' + activePlayer).style.display = 'block';
      document.getElementById('error-' + activePlayer).textContent = 'Rolled a 1! :(';
      nextPlayer();
    } else if (dice1 === 6 && dice2 === 6) {
      document.getElementById('error-' + activePlayer).style.display = 'block';
      document.getElementById('error-' + activePlayer).textContent = 'Rolled two 6\'s! :(';
      scores[activePlayer] = 0;
      document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];
      nextPlayer();
    } else {
      roundScore += (dice1 + dice2);
      document.querySelector('#current-' + activePlayer).textContent = roundScore;
    }
    document.getElementById('error-' + activePlayer).style.display = 'none';
  }
});


document.querySelector('.btn-hold').addEventListener('click', function () {
  if (gamePlaying) {
    // Add CURRENT score to GLOBAL score
    scores[activePlayer] += roundScore;

    // Update the UI
    document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];

    // Check if player won the game
    if (scores[activePlayer] >= finalScore) {
      document.querySelector('#name-' + activePlayer).textContent = 'Winner!';
      document.getElementById('dice-1').style.display = 'none';
      document.getElementById('dice-2').style.display = 'none';

      document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
      document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
      gamePlaying = false;
    } else {
      //Next player
      nextPlayer();
    }
  }
});


function nextPlayer(equalDices) {
  console.log(equalDices);
  //Next player
  activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
  roundScore = 0;

  document.getElementById('current-0').textContent = '0';
  document.getElementById('current-1').textContent = '0';

  document.querySelector('.player-0-panel').classList.toggle('active');
  document.querySelector('.player-1-panel').classList.toggle('active');

  //document.querySelector('.player-0-panel').classList.remove('active');
  //document.querySelector('.player-1-panel').classList.add('active');

  // document.getElementById('dice-1').style.display = 'none';
  // document.getElementById('dice-2').style.display = 'none';

}

document.querySelector('.btn-new').addEventListener('click', init);

//Set players name
document.getElementById('input-0').addEventListener('keypress', function (e) {
  const input1 = document.getElementById("input-0").value
  if (e.keyCode === 13) {
    document.getElementById("name-0").innerHTML = input1;
    document.getElementById('input-0').style.display = 'none';
    document.getElementById('name-0').style.display = 'block';
    document.getElementById("input-1").focus();
  }
}, false);

document.getElementById('input-1').addEventListener('keypress', function (e) {
  const input2 = document.getElementById("input-1").value
  if (e.keyCode === 13) {
    document.getElementById("name-1").innerHTML = input2;
    document.getElementById('input-1').style.display = 'none';
    document.getElementById('name-1').style.display = 'block';
  }
}, false);

function init() {
  scores = [0, 0];
  activePlayer = 0;
  roundScore = 0;
  gamePlaying = true;


  document.getElementById('dice-1').style.display = 'none';
  document.getElementById('dice-2').style.display = 'none';

  document.getElementById('error-0').style.display = 'none';
  document.getElementById('error-1').style.display = 'none';

  document.getElementById('score-0').textContent = '0';
  document.getElementById('score-1').textContent = '0';
  document.getElementById('current-0').textContent = '0';
  document.getElementById('current-1').textContent = '0';
  document.getElementById('input-0').style.display = 'block';
  document.getElementById("input-0").value = '';
  document.getElementById('input-1').style.display = 'block';
  document.getElementById("input-1").value = '';

  document.getElementById('final-score-input').style.display = 'block';
  document.getElementById('final-score-label').style.display = 'block';
  document.getElementById('final-score-input').value = '';
  document.getElementById('final-score-text').style.display = 'none';

  document.getElementById('name-0').style.display = 'none';
  document.getElementById('name-1').style.display = 'none';
  document.querySelector('.player-0-panel').classList.remove('winner');
  document.querySelector('.player-1-panel').classList.remove('winner');
  document.querySelector('.player-0-panel').classList.remove('active');
  document.querySelector('.player-1-panel').classList.remove('active');
  document.querySelector('.player-0-panel').classList.add('active');
}

document.getElementById('final-score-input').addEventListener('keypress', function (e) {
  finalScore = document.getElementById("final-score-input").value
  if (e.keyCode === 13) {
    document.getElementById("final-score-text").innerHTML = `The final score is set to: ${finalScore}`;
    document.getElementById('final-score-input').style.display = 'none';
    document.getElementById('final-score-label').style.display = 'none';
    document.getElementById('final-score-text').style.display = 'block';
  }
}, false);

/*
YOUR 3 CHALLENGES
Change the game to follow these rules:
1. A player looses his ENTIRE score when he rolls two 6 in a row.
 After that, it's the next player's turn. 
 (Hint: Always save the previous dice roll in a separate variable)
2. Add an input field to the HTML where players can set the winning score, 
so that they can change the predefined score of 100. (Hint: you can read that value with the 
  .value property in JavaScript. This is a good oportunity to use google to figure this out :)
3. Add another dice to the game, so that there are two dices now. 
The player looses his current score when one of them is a 
1. (Hint: you will need CSS to position the second dice, 
  so take a look at the CSS code for the first one.)
*/