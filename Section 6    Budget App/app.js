////////////////////////////////// budget Controller ////////////////////////////////////////////////
const budgetController = (function () { // Module

  const Expense = function (id, description, value) {
    this.id = id;
    this.description = description;
    this.value = value;
    this.percentage = -1;
  }

  Expense.prototype.calcPercentages = function (totalIncome) {
    if (totalIncome > 0) {
      this.percentage = Math.round((this.value / totalIncome) * 100)
      console.log(this.percentage)
    } else this.percentage = -1
  }

  Expense.prototype.getPercentage = function () {
    return this.percentage;
  }

  const Income = function (id, description, value) {
    this.id = id;
    this.description = description;
    this.value = value;
  }

  const calcTotal = function (type) {
    let sum = 0;
    data.allItems[type].forEach(function (curr) {
      sum = sum + curr.value;
    })
    data.totals[type] = sum;
  }

  const data = {
    allItems: {
      exp: [],
      inc: [],
    },
    totals: {
      exp: 0,
      inc: 0
    },
    budget: 0,
    percentage: -1, // -1 means it doesn't exist.
  }

  return {
    addItem: function (type, description, value) {
      let newItem, ID;

      ///////////// https://tylermcginnis.com/computed-property-names /////////////////

      if (data.allItems[type].length > 0) {
        ID = data.allItems[type][data.allItems[type].length - 1].id + 1;
      } else {
        ID = 0
      }

      if (type === 'exp') {
        newItem = new Expense(ID, description, value)
      } else if (type === 'inc') {
        newItem = new Income(ID, description, value)
      }

      data.allItems[type].push(newItem);
      return newItem;
    },

    calcBudget: function () {
      // calc total exp and inc
      calcTotal('exp'),
        calcTotal('inc'),

        //calc the budget
        data.budget = data.totals.inc - data.totals.exp;

      //calc the percentage of the income that we spent
      if (data.totals.inc > 0) {
        data.percentage = Math.round((data.totals.exp / data.totals.inc) * 100);
      }
    },

    calcPercentages: function () {
      data.allItems.exp.forEach(item => {
        item.calcPercentages(data.totals.inc);
      })
    },

    getPercentages: function () {
      const allPerc = data.allItems.exp.map(item => {
        return item.getPercentage();
      })
      console.log(allPerc);
      return allPerc
    },

    deleteItem: function (type, id) {
      const ids = data.allItems[type].map(item => {
        return item.id
      })

      const index = ids.indexOf(id);

      if (index !== -1) {
        data.allItems[type].splice(index, 1);
      }
      console.log(ids)
    },

    getBudget: function () {
      return {
        budget: data.budget,
        totalIncomes: data.totals.inc,
        exp: data.totals.exp,
        percentage: data.percentage,
      }
    },
  }
})();


/////////////////////////////////////////////// UI Controller ////////////////////////////////////////////////
const UIController = (function () { // Module

  const DOMStrings = {
    inputType: '.add__type',
    inputDescription: '.add__description',
    inputValue: '.add__value',
    inputBtn: '.add__btn',
    incomeContainer: '.income__list',
    expensesContainer: '.expenses__list',
    budgetValue: '.budget__value',
    budgetIncome: '.budget__income--value',
    budgetExpense: '.budget__expenses--value',
    budgetExpensePercentage: '.budget__expenses--percentage',
    itemPercentage: '.item__percentage',
    listContainer: '.container',
    date: '.budget__title--month',
  }


  const formatNumber = (num, type) => {
    num = Math.abs(num)
    num = num.toFixed(2)

    const numSplit = num.split('.')

    let int = numSplit[0]

    if (int.length > 3) {
      int = `${int.substr(0, int.length - 3)},${int.substr(int.length - 3, 3)}`
    }

    const dec = numSplit[1]

    return (type === 'exp' ? '-' : '+') + ' ' + int + '.' + dec;
  }

  const nodeListForEach = (list, callback) => {
    for (let i = 0; i < list.length; i++) {
      callback(list[i], i)
    }
    console.log({
      list
    })
  }

  return {
    getInput: function () {
      return {
        type: document.querySelector(DOMStrings.inputType).value,
        description: document.querySelector(DOMStrings.inputDescription).value,
        value: parseFloat(document.querySelector(DOMStrings.inputValue).value)
      }
    },

    addListItem: function (obj, type) {
      let html, newHTML, element;

      //create HTML string with placeholder text
      if (type === 'exp') {
        element = DOMStrings.expensesContainer;

        html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div>' +
          '<div class="item__percentage"></div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
      } else if (type === 'inc') {
        element = DOMStrings.incomeContainer;

        html = '<div class="item clearfix" id="inc-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div>' +
          '<div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
      }

      //replace the placeholder with the actual data
      newHTML = html.replace('%id%', obj.id);
      newHTML = newHTML.replace('%description%', obj.description)
      newHTML = newHTML.replace('%value%', formatNumber(obj.value, type))

      //insert the HTML into the DOM //////// Insert adjacent HTML ///////////
      document.querySelector(element).insertAdjacentHTML('beforeend', newHTML);
    },

    deleteListItem: function (selectorID) {
      const el = document.getElementById(selectorID)
      el.parentNode.removeChild(el);
    },

    clearFields: function () {
      const fields = document.querySelectorAll(DOMStrings.inputDescription + ', ' + DOMStrings.inputValue);

      //// make an array out of a (node)list

      // const fieldsArray = Array.prototype.slice.call(fields) 

      fields.forEach(function (current, index, array) {
        //console.log(current, index, array) // we have access to this variables
        current.value = '';
      })

      fields[0].focus();
    },

    displayBudget: function (obj) {
      let type;
      obj.budget > 0 ? type = 'inc' : type = 'exp';

      document.querySelector(DOMStrings.budgetValue).textContent = obj.budget !== 0 ? formatNumber(obj.budget, type) : obj.budget;
      document.querySelector(DOMStrings.budgetIncome).textContent = formatNumber(obj.totalIncomes, 'inc');
      document.querySelector(DOMStrings.budgetExpense).textContent = formatNumber(obj.exp, 'exp');

      if (obj.percentage > 0) {
        document.querySelector(DOMStrings.budgetExpensePercentage).textContent = `${obj.percentage}%`;
        document.querySelector(DOMStrings.itemPercentage).textContent = `${obj.percentage}%`;
      } else {
        '---'
      }
    },

    displayPercentages: function (percentages) {
      const fields = document.querySelectorAll(DOMStrings.itemPercentage);

      nodeListForEach(fields, function (curr, index) {
        curr.textContent = percentages[index] > 0 ?
          `${percentages[index]}%` :
          `---`
      })
    },

    displayDate: function () {
      const now = new Date();
      const year = now.getFullYear();
      const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'December']
      const month = now.getMonth();
      document.querySelector(DOMStrings.date).textContent = `${months[month]} ${year}`;
    },

    changedType: function () {
      const fields = document.querySelectorAll(DOMStrings.inputType + ',' + DOMStrings.inputDescription + ',' + DOMStrings.inputValue)
      nodeListForEach(fields, function (cur) {
        cur.classList.toggle('red-focus')
      })
      document.querySelector(DOMStrings.inputBtn).classList.toggle('red');
    },

    getDOMStrings: function () {
      return DOMStrings;
    }
  }

})();

////////////////////////////////////////////////// global controller ////////////////////////////////////////////////
const controller = (function (budgetCtrl, UICtrl) {
  // budgetController.func() Make it less independent
  // budgetCtrl.func(5) // Should be like this

  const setupEventListeners = function () {
    const DOM = UICtrl.getDOMStrings();

    document.querySelector(DOM.inputBtn).addEventListener('click', controllAddItem);

    document.addEventListener('keypress', function (e) {
      if (e.key === 'Enter' || e.keyCode === 13 || event.which === 13) {
        controllAddItem();
      }
    })

    document.querySelector(DOM.listContainer).addEventListener('click', ctrlDeleteItem)
    document.querySelector(DOM.inputType).addEventListener('change', UICtrl.changedType)
  }

  const updateBudget = function () {
    //calc the budget
    budgetCtrl.calcBudget();

    //return the budget
    const budget = budgetCtrl.getBudget();

    //Display it to the UI
    UICtrl.displayBudget(budget);
  }

  const updatePercentages = function () {
    budgetCtrl.calcPercentages();
    const percentages = budgetCtrl.getPercentages();
    // UICtrl.displayPercentages(percentages)
  }

  const controllAddItem = function () {
    let input, newItem;

    input = UICtrl.getInput();

    if (input.description !== '' && input.value !== NaN && input.value > 0) {
      newItem = budgetCtrl.addItem(input.type, input.description, input.value);
      UICtrl.addListItem(newItem, input.type);
      UIController.clearFields();
      updateBudget();
      // updatePercentages();

    } else throw new Error('Please fill out all the fields!')

    console.log(newItem)
  }

  const ctrlDeleteItem = function (e) {
    let itemID, splitID, type, id;

    itemID = event.target.parentNode.parentNode.parentNode.parentNode.id;

    if (itemID) {
      splitID = itemID.split('-');
      type = splitID[0];
      id = parseInt(splitID[1]);

      budgetCtrl.deleteItem(type, id);
      UICtrl.deleteListItem(itemID);
      updateBudget();
      updatePercentages();
    }
  }

  return { // all the codes gets executed when the app starts
    init: function () {
      setupEventListeners();
      UICtrl.displayBudget({
        budget: 0,
        totalIncomes: 0,
        exp: 0,
        percentage: '--',
      });
      UIController.displayDate();
      console.log('Application has started!')
    }
  }

})(budgetController, UIController);

controller.init();