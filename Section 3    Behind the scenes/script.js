/////////// HOISTING


//////////////////////////////////////// FUNCTIONS ////////////////////////////////////////////////

//works because of hoisting. So the function has been declared before execution. 
//calcAge(1991); 

//Hoisting works because it's a function declaration.
function calcAge(year) {
  console.log(2020 - year);
}

// calcAge(1992)

//retirement(1992); //Hoisting doesn't work because it's a function expression.

const retirement = (year) => {
  console.log(65 - (2020 - year))
}

//works only after the function has been declared.
//retirement(1992); 

//////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////// VARIABLES ////////////////////////////////////////////////

// It's been set to undefined if it's declared with var, but doesn't work with const or with let.
// console.log(age); 
const age = 28;

//work OFC
// console.log(age); 

//works both ways
// foo();

function foo() {
  const age = 28;
  console.log(age);
}

//works both ways
//foo();                  // logs the age from the function own execution context
//console.log(age);       // logs the age from the global execution object



////////////////////////////////////////////////////////////////////////////////////////////////

/////////// SCOPING

const a = "Hello";
//first();

function first() {
  const b = ' - ';
  second();

  function second() {
    const c = 'Bello'
    third(); // possible because of the scope chain
    // console.log(a + b + c);
  }
}

function third() {
  const d = '!';
  console.log(a, d); //works because of the scope chain.
  //console.log(c); //throws an error because the EC is different to the Scope Chain!!!
}


/////////// THIS Keyword 


// In the global execution context its logs the global/window object.
//console.log(this); 

function calcAge(year) {
  console.log(2020 - year);
  console.log(this); //points to the globbal/window object
}

// calcAge(1992);

const john = {
  name: 'John',
  yearOfBirth: 1992,
  calcAge: function () {
    console.log(this.name, 2020 - this.yearOfBirth)
    console.log(this); // logs this obj because this is refers to this obj and because it's a method.

    /*
    function innerFunc() {
      console.log(this); // logs the window object because it's a regular function call.
    }
    innerFunc(); 
    */
  }
}

john.calcAge();


const mike = {
  name: 'Mike',
  yearOfBirth: 1991,
}

//method borrowing
mike.calcAge = john.calcAge; // works because the this keyword becomes something as soon as the method gets called!

mike.calcAge()